# Game

A small game adapted from a [python implementation](http://perso.eleves.ens-rennes.fr/people/pierre.le-barbenchon/ezdiojea.html) of Pierre Le Barbenchon.

# Piano

Small piano to demonstrate how to use an audio in js. The piano notes are from [fuhton](https://github.com/fuhton/piano-mp3).

## Contributors

- Pierre Le Barbenchon
- Robin Camarasa
